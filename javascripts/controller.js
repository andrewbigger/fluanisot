/**
 * FluAnisot Simulation Controller
 * Controls timing and execution of demonstration scripts and UI data input and output
 * @author Andrew Bigger and Stephen Bigger
 * @version  0.8
 * @since    0.1
 *
 * @requires jquery 1.7.2
 *
 * @date 09/04/2012
 */

	var writer;
	var viscTable;

	var tempSelect;
	var viscFSelect;
	var exPolSelect = 'v';
	var emPolSelect = 'v';

	function loadDemo()
	{
		//loads viscocity table into memory
		viscTable = calc_visc_table();

		set_viscosity_temp();
	}

	function toggle_demo()
	{
		//starts or stops demo depending on tatus
		if (get_element_by_id("startStop").value=="Start Simulation")
		
		{
			get_element_by_id("startStop").value="Stop Simulation";
			start_demo();

		} else {

			get_element_by_id("startStop").value="Start Simulation";
			stop_demo();
		}	
	}

	function start_demo()
	{
		//start timer
		//attach set reading routine to tick event
		writer = self.setInterval("set_reading()",999);
	}

	function stop_demo()
	{
		//destroy timer and reset document
		clearInterval(writer);
		set_element_by_id("reading", "");
		
	}


	function reset_reading()
	{
		//clear timer
		clearInterval(writer);
		//check for changes to the selected temperature
		tempSelect = $( "#tempC" ).slider( "option", "values" );
		//start another timer
		writer = self.setInterval("set_reading()", Math.random()*2000);


	}
	
	function get_reading()
	{
		//write results to preview and lab book table
		set_element_by_id("results", get_element_by_id("results").innerHTML + to_tr());
		
		$("#success").fadeIn('slow');
		$("#success").animate({opacity: 1.0}, 500);
		$("#success").fadeOut('slow');
	
	}
	
	
	function get_element_by_id(id)
	{
		return document.getElementById(id);
	}
	
	function set_viscosity_temp()
	{
		//grabs slider value (temperature)
		viscTable = calc_visc_table();
		tempSelect = ($("#tempC").slider('value'));
		//viscocity grabbed from table uing user selected temperature
		viscFSelect = viscTable[tempSelect];
	
	    //update document
		document.simulation.temp.value = tempSelect;
		document.simulation.viscCP.value = viscFSelect.toString();
		
	}
	
	function set_filter_orientation(filterTag) 
	{
		//set polarisation filter orientation on screen
		var filterIcon = get_element_by_id(filterTag);
		var filterLabel = '<p>excitation polarizer</p>';
		
		
		if(filterTag == 'em')
		{
			filterLabel = '<p>emission polarizer</p>';
		}
		
		var orientation;
	
		if(filterIcon.innerHTML == '<img src="images/vertical-filter.png">')
		{
			filterIcon.innerHTML = '<img src="images/horizontal-filter.png">';
			orientation = 'h';
		}
		else
		{
			filterIcon.innerHTML = '<img src="images/vertical-filter.png">';
			orientation = 'v';
		}
	
		//sets polarisation filter orientation in memory
	
		if(filterTag == 'em')
		{
			emPolSelect = orientation;
		}
		else
		{
			exPolSelect = orientation;
		}
		
	}
	
	function set_reading()
	{
		//show user a fluctuated reading
		//set off by the writer timer
		
		readingLabel = '<p>I<sub>'+ exPolSelect.toUpperCase() + emPolSelect.toUpperCase()+"</sub> = "
		
		//if user has changed visc value then update
		if (Number(document.simulation.viscCP.value) != viscFSelect){
			viscFSelect = document.simulation.viscCP.value;
		}
		
		get_element_by_id('reading').innerHTML = readingLabel + fluctuate_reading(
		
																Number(tempSelect),
																exPolSelect,
																emPolSelect,
																Number(viscFSelect)
	
															 ).toFixed(1) + '</p>';
	
		
		reset_reading();
	}
		
	
	function set_element_by_id(id, html)
	{
		document.getElementById(id).innerHTML = html;
	}
	
	function print_page()
	{
	 	window.print()
	}
	
	function clear_labbook()
	{
		//nuke lab book
		set_element_by_id("results", blank_lab_book());
	}
	
	function blank_lab_book(){
		
		return "<tr><th>T/C</th><th>Visc/CP</th><th>I(vv)</th><th>I(vh)</th><th>I(hh)</th><th>I(hv)</th></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
		
	}