/**
 * FluAnisot Intensity Calculation
 * Calculates and fluctuates intensity for simulation
 * @author Andrew Bigger and Stephen Bigger
 * @version  0.8
 * @since    0.1
 *
 * @requires jquery 1.7.2
 *
 * @date 09/04/2012
 */

	var viscF;
	var tempCSelect;
	var exPol;
	var emPol;

	var fluctPercent = 0.75;
	var Int;
	var visc;
	var ivv;
	var ivh;
	var ihh;
	var ihv;
	var sign;
	var fluct;

	function fluctuate_reading(temp, ex, em, vF)
	{

		tempCSelect = Number(temp);
		exPol = ex;
		emPol = em;
		viscF = Number(vF);

		calc_intensity(); //run intensity calculation

		if ((Math.random()*2 >= 1)) //randomize sign
		{
			sign = 1;
		} 
		else
		{
			sign = -1;
		}

		//determine fluctuation 'error'
		fluct = (fluctPercent/100)*Int*(Math.random()*10)/10;
		return (Int + sign * fluct); //introduce fluct 'error' to result
	}

	function calc_intensity()
	{
		//intensity calculation constants
		var Rx = 8.314;
		var tf = 15.6e-09;
		var vM = 0.132;
		var a0 = 0.4;
		var g1 = 0.978;
		var iParaM = -3.3250e-04;
		var iPara0 = +2.8730e+02;

		visc = 1e-03*viscF;

		//intensity calculations
		var a1 = a0*vM*visc/(vM*visc+3*tf*Rx*(tempCSelect+273.15));
		var iPara = (iPara0+iParaM*(tempCSelect+273.15)/visc);
		var iPerp = iPara*(1-a1)/(g1*(1+2*a1));

		ivv = iPara;
	    ivh = iPerp;
	    ihh = iPara;
        ihv = g1*ihh;

        //determine which combination of filter polarisation is selected
        //and therefore determine which result to return to the user.
		if(exPol=='v' && emPol=='v')
		{
			Int = ivv;
		}

		if(exPol=='v' && emPol=='h')
		{
			Int = ivh;
		}

		if(exPol=='h' && emPol=='h')
		{
			Int = ivv;
		}

		if(exPol=='h' && emPol=='v')
		{
			Int = ihv;
		}


	}
	
	function to_tr()
	{
		//compiles results into a table row for display in lab book
		
		var tc = (Int + sign * fluct);

		var data = '<tr><td>'+tc.toFixed(1)+'</td><td>'+viscF.toFixed(1)+'</td><td>'+ivvt()+'</td><td>'+ivht()+'</td><td>'+ihht()+'</td><td>'+ihvt()+'</td></tr>';
      	
		return data;
	}
	
	function ivvt()
	{
		var data = "&nbsp;"
		if(exPol=='v' && emPol=='v')
		{
			data = ivv.toFixed(1)
		}
		
		return data
	}
	
	function ivht()
	{
		var data = ""
		if(exPol=='v' && emPol=='h')
		{
			data = ivh.toFixed(1)
		}
		
		return data
	}
	
	function ihht()
	{
		var data = ""
		if(exPol=='h' && emPol=='h')
		{
			data = ihh.toFixed(1)
		}
		
		return data
	}
	
	function ihvt()
	{
		var data = ""
		if(exPol=='h' && emPol=='v')
		{
			data = ihv.toFixed(1)
		}
		
		return data
	}	

	function calc_visc_table()
	{

		//viscocity table
		var viscValues= new Array(69);

		viscValues[10] = 1.307;
		viscValues[11] = 1.271;
		viscValues[12] = 1.235;
		viscValues[13] = 1.202;
		viscValues[14] = 1.169;
		viscValues[15] = 1.139;
		viscValues[16] = 1.109;
		viscValues[17] = 1.081;
		viscValues[18] = 1.053;
		viscValues[19] = 1.027;
		viscValues[20] = 1.002;
		viscValues[21] = 0.9779;
		viscValues[22] = 0.9548;
		viscValues[23] = 0.9325;
		viscValues[24] = 0.9111;
		viscValues[25] = 0.8904;
		viscValues[26] = 0.8705;
		viscValues[27] = 0.8513;
		viscValues[28] = 0.8327;
		viscValues[29] = 0.8148;
		viscValues[30] = 0.7975;
		viscValues[31] = 0.7808;
		viscValues[32] = 0.7647;
		viscValues[33] = 0.7491;
		viscValues[34] = 0.734;
		viscValues[35] = 0.7194;
		viscValues[36] = 0.7052;
		viscValues[37] = 0.6915;
		viscValues[38] = 0.6783;
		viscValues[39] = 0.6654;
		viscValues[40] = 0.6259;
		viscValues[41] = 0.6408;
		viscValues[42] = 0.6291;
		viscValues[43] = 0.6178;
		viscValues[44] = 0.6067;
		viscValues[45] = 0.596;
		viscValues[46] = 0.5856;
		viscValues[47] = 0.5755;
		viscValues[48] = 0.5656;
		viscValues[49] = 0.5561;
		viscValues[50] = 0.5468;
		viscValues[51] = 0.5378;
		viscValues[52] = 0.529;
		viscValues[53] = 0.5204;
		viscValues[54] = 0.5121;
		viscValues[55] = 0.504;
		viscValues[56] = 0.4961;
		viscValues[57] = 0.4884;
		viscValues[58] = 0.4809;
		viscValues[59] = 0.4736;
		viscValues[60] = 0.4665;
		viscValues[61] = 0.4596;
		viscValues[62] = 0.4528;
		viscValues[63] = 0.4462;
		viscValues[64] = 0.4398;
		viscValues[65] = 0.4335;
		viscValues[66] = 0.4273;
		viscValues[67] = 0.4213;
		viscValues[68] = 0.4155;
		viscValues[69] = 0.4098;
		viscValues[70] = 0.4042;

		return viscValues;
	}