/**
 * FluAnisot UI event handlers
 * @author Andrew Bigger
 * @version  0.3
 * @since    0.1
 *
 * @requires jquery 1.7.2
 *
 * @date 09/06/2012
 */

$(document).ready(function(){

//Sets up default state of items on screen
  $("#theory").hide();
  $("#lab-book").hide();
  $("#experiment").hide();
  $("#simulation").hide();
  $("#success").hide();
  $("#warning").hide();
  $(".content").animate({'width': '35%'})
  
  
  $('.toggle-fullscreen').click(function(){
  	if ($('#simulation').is (':visible')){
  		$('#simulation').hide(600);
  		$('.content').animate({'width': '75%'},600);
  	} 
  	if (!$('#simulation').is (':visible')){
	  	$('#simulation').show(600);
  		$('.content').animate({'width': '35%'},600);
  		$('#toggle-fullscreen').animate({'width': "16px"},0)
  	}

    });

//Jquery slider (cross browser compatible) and default settings
   $("#tempC").slider(
   {
     min:10,
     max:70,
     value:25,
     slide:function(event,ui) 
     {
       set_viscosity_temp(); //on change update reading
     }
  });

//Once document is loaded, start up animation of theory and simulation divs
  $("#theory").toggle(400);
  $("#simulation").toggle(400);

//Menu button event handlers
  $('#theory-button').click(function(){
    if (!$('#theory').is(':visible')) {
      hide_content();
      $("#theory").toggle(400);
    }
  });

  $('#lab-book-button').click(function(){
    if (!$('#lab-book').is(':visible')) {
      hide_content();
      $("#lab-book").toggle(400);
    }
  });

  $('#experiment-button').click(function(){
    if (!$('#experiment').is(':visible')) {
      hide_content();
      $("#experiment").toggle(400);
    };
    

  });

function hide_content()
{
//Toggles each content pane if visible is true
  if ($('#theory').is(':visible')) {
    $('#theory').toggle(400);
}

  if ($('#experiment').is(':visible')) {
    $('#experiment').toggle(400);
  }

  if ($('#lab-book').is(':visible')) {
    $('#lab-book').toggle(400);
  }

}

});