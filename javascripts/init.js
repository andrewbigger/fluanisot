Shadowbox.init();

tinyMCE.init({
				mode : "textareas",
				theme : "advanced",
				plugins : "print",
				theme_advanced_toolbar_location : "top",
				theme_advanced_buttons1 : "print,font-size,bold,italic,underline,strikethrough,removeformat,separator,outdent,indent,hr,separator,forecolor,sub,sup,charmap,separator,undo,redo,cleanup,separator,bullist,numlist",
				theme_advanced_buttons2 : "",
				theme_advanced_buttons3 : ""
			});

function run_init()
{
	loadDemo(); 
	init();

}